minetest mod moreblocks lite
============================

Moreblocks mod for Minetest <http://minetest.net>, a free/libre infiniteworld block sandbox game.

Moreblocks mod is orginaly written by Calinou.
Moreblocks offers the same blocks and API except the slopes.
slopes are deleted so this is a lite version.

To install, just download this repository into your "mods" directory and rename into "moreblocks"

More Blocks code is licensed under the zlib license, textures are by Calinou and are licensed under CC BY-SA 3.0 Unported.

**Forum topic:** <https://forum.minetest.net/viewtopic.php?f=11&t=509>
